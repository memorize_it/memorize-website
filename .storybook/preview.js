// Import global css here
import '../src/styles/globals.scss';
import { MINIMAL_VIEWPORTS } from '@storybook/addon-viewport';
import { withNextRouter } from 'storybook-addon-next-router';
import { addDecorator } from '@storybook/react';

export const parameters = {
  viewport: {
    viewports: MINIMAL_VIEWPORTS,
  },
};

addDecorator(
  withNextRouter({
    path: '/', // defaults to `/`
    asPath: '/', // defaults to `/`
    query: {}, // defaults to `{}`
    push() {}, // defaults to using addon actions integration, can override any method in the router
  }),
);
