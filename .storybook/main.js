const path = require('path');

module.exports = {
  core: {
    builder: "webpack5",
  },
  stories: ['../src/components/**/*.stories.tsx'],
  // Add nextjs preset
  presets: [path.resolve(__dirname, './next-preset.js')],
  addons: ['@storybook/addon-essentials'],
};
