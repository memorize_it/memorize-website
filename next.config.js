/* eslint-disable @typescript-eslint/no-var-requires */
//next.config.js
const BrotliPlugin = require('brotli-webpack-plugin');
const withBundleAnalyzer = require('@next/bundle-analyzer')({
  enabled: process.env.ANALYZE === 'true',
});

const assetPrefix = process.env.ASSET_PREFIX ? process.env.ASSET_PREFIX : '';
const basePath = process.env.BASE_PATH ? process.env.BASE_PATH : '';

module.exports = withBundleAnalyzer({
  future: {
    webpack5: true,
  },
  assetPrefix,
  basePath,
  env: {
    assetPrefix,
    basePath,
  },
  webpack: function (config, { dev, isServer }) {
    if (!dev) {
      config.plugins.push(
        new BrotliPlugin({
          asset: '[path].br[query]',
          test: /\.(js|css|html|svg)$/,
          threshold: 10240,
          minRatio: 0.8,
        }),
      );
    }

    config.module.rules.push({
      test: /\.md$/,
      use: 'raw-loader',
    });

    if (!isServer) {
      config.resolve.fallback.fs = false;
    }

    return config;
  },
});
