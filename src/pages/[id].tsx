import React from 'react';
import Layout from '../components/layout/Layout';
import Zettel from '../components/zettel/Zettel';
import config from '../data/config';
import { loadLocalZettelkasten } from '../data/zettelkasten';
import { GetStaticProps, GetStaticPaths } from 'next';
import { ZettelWithContentInterface } from '@memorize/types';
import { getLocalZettelById } from '../data/zettel';
import ZettelList from '../components/zettel-list/ZettelList';
import { displayOneLineZettel } from '../components/zettel-list-compact-item/ZettelListCompactItem';
import labels from '../data/labels';
import Link from 'next/link';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faGlobe } from '@fortawesome/free-solid-svg-icons';
import { splitZettel } from '@memorize/file-system';
import TagCloud from '../components/tag-cloud/TagCloud';

export interface ZettelTemplateInterface extends ZettelWithContentInterface {
  siteTitle: string;
  siteDescription: string;
  body: string;
  byZettels: ZettelWithContentInterface[];
  toZettels: ZettelWithContentInterface[];
}

export default function ZettelTemplate({
  siteTitle,
  siteDescription,
  body,
  id,
  tags,
  byZettels,
  toZettels,
}: ZettelTemplateInterface): JSX.Element {
  const {
    references: { by, byEmpty, to, toEmpty },
    seeGraph,
    tags: { tagsTitle, tagsEmpty },
  } = labels.pages.zettel;
  return (
    <Layout siteTitle={siteTitle} siteDescription={siteDescription}>
      <div className="container">
        <div className="row p-4 h-100">
          <div className="col-md-8">
            <Zettel markdownBody={body}></Zettel>
          </div>
          <div className="col-md-4 d-flex flex-column justify-content-between">
            <Link href={`/graph/${id}`}>
              <a className="btn btn-primary text-white" role="button">
                <div>
                  <FontAwesomeIcon icon={faGlobe} />
                </div>
                <span>{seeGraph}</span>
              </a>
            </Link>

            <div className="justify-content-center w-100">
              <h2 className="text-center">{tagsTitle}</h2>
              {tags.length ? <TagCloud tags={tags} /> : <p>{tagsEmpty}</p>}
            </div>
            <ZettelList
              title={by}
              emptyList={byEmpty}
              customRenderer={displayOneLineZettel}
              zettels={byZettels}
            ></ZettelList>
            <ZettelList
              title={to}
              emptyList={toEmpty}
              customRenderer={displayOneLineZettel}
              zettels={toZettels}
            ></ZettelList>
          </div>
        </div>
      </div>
    </Layout>
  );
}

export const getStaticProps: GetStaticProps<ZettelTemplateInterface> = async function getStaticProps({
  params,
}) {
  if (!params) {
    throw new Error('[id].tsx(getStaticProps): params undefined');
  }

  const { id } = params;
  if (typeof id !== 'string') {
    throw new Error('[id].tsx(getStaticProps): multiple id defined');
  }
  const numberId = Number.parseInt(id);
  const zettel = await getLocalZettelById(numberId);

  if (!zettel) {
    throw new Error(
      `[id].tsx(getStaticProps): unable to retrieve zettle with id "${id}".`,
    );
  }

  if (!zettel.content) {
    throw new Error(`[id].tsx(getStaticProps): zettel content is empty.`);
  }

  const { content, references, relatedZettels } = zettel;
  const { body } = splitZettel(content);
  return {
    props: {
      ...zettel,
      siteTitle: config.title,
      siteDescription: config.description,
      body,
      byZettels: await Promise.all(
        relatedZettels
          .filter(({ references: zReferences }) =>
            zReferences.includes(numberId),
          )
          .map(async ({ id: zId }) => await getLocalZettelById(zId)),
      ),
      toZettels: await Promise.all(
        relatedZettels
          .filter(({ id: zId }) => references.includes(zId))
          .map(async ({ id: zId }) => await getLocalZettelById(zId)),
      ),
    },
  };
};

export const getStaticPaths: GetStaticPaths = async function getStaticPaths() {
  const zettelkasten = await loadLocalZettelkasten();
  const paths = zettelkasten.zettels.map(({ id }) => ({
    params: {
      id: id.toString(),
    },
  }));

  return {
    paths,
    fallback: false,
  };
};
