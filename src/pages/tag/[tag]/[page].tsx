import React from 'react';
import Layout from '../../../components/layout/Layout';
import config from '../../../data/config';
import { GetStaticPaths, GetStaticProps } from 'next';
import { loadLocalZettelkasten } from '../../../data/zettelkasten';
import {
  ZettelkastenInterface,
  ZettelWithContentInterface,
} from '@memorize/types';
import ZettelPaginatedList from '../../../components/ZettelPaginatedList';
import { HeaderInterface } from '../../../components/header/Header';
import { getLocalZettelById } from '../../../data/zettel';

export interface TagPageTemplateInterface {
  siteTitle: string;
  siteDescription: string;
  title: string;
  description: string;
  tag: string;
  zettels: ZettelWithContentInterface[];
  currentPage: number;
  pageCount: number;
  header: HeaderInterface;
}

function getZettelPages(
  zettelkasten: ZettelkastenInterface,
  tag: string,
): number {
  return Math.ceil(
    zettelkasten.zettels.filter(({ tags }) => tags.includes(tag)).length /
      config.zettelPerPage,
  );
}

export default function TagPageTemplate(
  props: TagPageTemplateInterface,
): JSX.Element {
  return (
    <Layout
      siteTitle={props.siteTitle}
      siteDescription={props.siteDescription}
      header={props.header}
    >
      <ZettelPaginatedList
        zettels={props.zettels}
        currentPage={props.currentPage}
        pageCount={props.pageCount}
        basePath={`/tag/${props.tag}`}
      />
    </Layout>
  );
}

export const getStaticProps: GetStaticProps<TagPageTemplateInterface> = async function getStaticProps({
  params,
}) {
  if (!params) {
    throw new Error('[tag]/[page].tsx(getStaticProps): params undefined');
  }

  const { tag, page } = params;
  if (typeof tag !== 'string') {
    throw new Error('[tag]/[page].tsx(getStaticProps): multiple tags defined');
  }

  if (typeof page !== 'string') {
    throw new Error('[tag]/[page].tsx(getStaticProps): multiple pages defined');
  }

  const zettelkasten = await loadLocalZettelkasten();
  const currentPage = Number.parseInt(page);
  const totalPages = getZettelPages(zettelkasten, tag);

  if (currentPage > totalPages) {
    throw new Error(
      '[tag]/[page].tsx(getStaticProps): currentPage is too large',
    );
  }

  const zettels = await Promise.all(
    zettelkasten.zettels
      .filter((z) => z.tags.includes(tag))
      .slice(
        currentPage * config.zettelPerPage,
        (currentPage + 1) * config.zettelPerPage,
      )
      .map(({ id }) => getLocalZettelById(id)),
  );

  return {
    props: {
      siteTitle: config.title,
      siteDescription: config.description,
      title: config.title,
      description: config.description,
      zettels: zettels,
      tag,
      currentPage,
      pageCount: totalPages,
      header: {
        title: `#${tag}`,
        subtitle: `${currentPage + 1}/${totalPages}`,
      },
    },
  };
};

export const getStaticPaths: GetStaticPaths = async function getStaticPaths() {
  const zettelkasten = await loadLocalZettelkasten();

  const paths = [];
  zettelkasten.meta.tags.forEach((tag: string) => {
    const totalPages = getZettelPages(zettelkasten, tag);
    for (let page = 0; page < totalPages; page++) {
      paths.push({
        params: {
          tag,
          page: page.toString(),
        },
      });
    }
  });

  return {
    paths,
    fallback: false,
  };
};
