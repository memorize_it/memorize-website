import { createZettelkasten } from '@memorize/core';
import { GetStaticPaths, GetStaticProps } from 'next';
import React from 'react';
import { VisualizationSpec } from 'react-vega';
import Graph, { createZettelkastenSpec } from '../../components/Graph';
import Layout from '../../components/layout/Layout';
import config from '../../data/config';
import { getLocalZettelById } from '../../data/zettel';
import { loadLocalZettelkasten } from '../../data/zettelkasten';

export interface ZettelGraphInterface {
  siteTitle: string;
  siteDescription: string;
  zettelTitle: string;
  spec: VisualizationSpec;
}

export const getStaticProps: GetStaticProps<ZettelGraphInterface> = async function getStaticProps({
  params,
}) {
  if (!params) {
    throw new Error('[id].tsx(getStaticProps): params undefined');
  }

  const { id } = params;
  if (typeof id !== 'string') {
    throw new Error('[id].tsx(getStaticProps): multiple id defined');
  }

  const zettel = await getLocalZettelById(Number.parseInt(id));

  if (!zettel) {
    throw new Error(
      `[id].tsx(getStaticProps): unable to retrieve zettle with id "${id}".`,
    );
  }

  const zettelkasten = createZettelkasten({
    zettels: [zettel, ...zettel.relatedZettels],
  });

  return {
    props: {
      zettelTitle: zettel.title,
      siteTitle: config.title,
      siteDescription: config.description,
      spec: createZettelkastenSpec(zettelkasten),
    },
  };
};

export default function ZettelGraph({
  siteDescription,
  siteTitle,
  zettelTitle,
  spec,
}: ZettelGraphInterface): JSX.Element {
  return (
    <Layout siteTitle={siteTitle} siteDescription={siteDescription}>
      <Graph subtitle={zettelTitle} spec={spec} />
    </Layout>
  );
}

export const getStaticPaths: GetStaticPaths = async function getStaticPaths() {
  const zettelkasten = await loadLocalZettelkasten();

  return {
    paths: zettelkasten.zettels.map(({ id }) => ({
      params: {
        id: id.toString(),
      },
    })),
    fallback: false,
  };
};
