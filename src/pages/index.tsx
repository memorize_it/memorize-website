import React from 'react';
import config from '../data/config';
import Layout from '../components/layout/Layout';
import ZettelList from '../components/zettel-list/ZettelList';
import { HeaderInterface } from '../components/header/Header';
import { GetStaticProps } from 'next';
import { loadLocalZettelkasten } from '../data/zettelkasten';
import labels from '../data/labels';
import { ZettelInterface, ZettelWithContentInterface } from '@memorize/types';
import { getLocalZettelById } from '../data/zettel';
import SearchBar from '../components/search-bar/SearchBar';
import { displayZettel } from '../components/zettel-list-item/ZettelListItem';
import { splitZettel } from '@memorize/file-system';

export interface IndexPageInterface {
  title: string;
  description: string;
  zettels: ZettelInterface[];
  topZettels: ZettelWithContentInterface[];
}

export const getStaticProps: GetStaticProps<IndexPageInterface> = async function getStaticProps() {
  const zettelkasten = await loadLocalZettelkasten();
  const zettels = zettelkasten.zettels;
  const topZettels = await Promise.all(
    zettels
      .map((zettel) => {
        const { id } = zettel;
        const score = zettels.filter((z) => z.references.includes(id)).length;
        return { ...zettel, score };
      })
      .sort((z1, z2) => z2.score - z1.score)
      .slice(0, config.topZettelNumber)
      .map(({ id }) => getLocalZettelById(id)),
  );

  return {
    props: {
      zettels,
      topZettels: topZettels.map((z) => ({
        ...z,
        content: splitZettel(z.content).body,
      })),
      title: config.title,
      description: config.description,
    },
  };
};

export default function IndexPage({
  title,
  description,
  zettels,
  topZettels,
}: IndexPageInterface): JSX.Element {
  const header: HeaderInterface = {
    title: title,
    subtitle: description,
  };

  const { top, search } = labels.pages.index;

  return (
    <Layout siteTitle={title} siteDescription={description} header={header}>
      <div className="col d-flex flex-column align-items-center">
        <div className="justify-content-center mb-4">
          <h2 className="text-center">{search}</h2>
          <SearchBar zettels={zettels}></SearchBar>
        </div>
        <ZettelList
          zettels={topZettels}
          title={top}
          customRenderer={displayZettel}
        />
      </div>
    </Layout>
  );
}
