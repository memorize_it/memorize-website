import React from 'react';
import Layout from '../../components/layout/Layout';
import config from '../../data/config';
import { GetStaticPaths, GetStaticProps } from 'next';
import { loadLocalZettelkasten } from '../../data/zettelkasten';
import {
  ZettelkastenInterface,
  ZettelWithContentInterface,
} from '@memorize/types';
import ZettelPaginatedList from '../../components/ZettelPaginatedList';
import { HeaderInterface } from '../../components/header/Header';
import { getLocalZettelById } from '../../data/zettel';

export interface AllPageTemplateInterface {
  siteTitle: string;
  siteDescription: string;
  title: string;
  description: string;
  zettels: ZettelWithContentInterface[];
  currentPage: number;
  pageCount: number;
  header: HeaderInterface;
}

function getZettelPages({ zettels }: ZettelkastenInterface): number {
  return Math.ceil(zettels.length / config.zettelPerPage);
}

export default function AllPageTemplate(
  props: AllPageTemplateInterface,
): JSX.Element {
  return (
    <Layout
      siteTitle={props.siteTitle}
      siteDescription={props.siteDescription}
      header={props.header}
    >
      <ZettelPaginatedList
        zettels={props.zettels}
        currentPage={props.currentPage}
        pageCount={props.pageCount}
        basePath="/all"
      />
    </Layout>
  );
}

export const getStaticProps: GetStaticProps<AllPageTemplateInterface> = async function getStaticProps({
  params,
}) {
  if (!params) {
    throw new Error('all/[page].tsx(getStaticProps): params undefined');
  }

  const { page } = params;
  if (typeof page !== 'string') {
    throw new Error('all/[page].tsx(getStaticProps): multiple pages defined');
  }

  const zettelkasten = await loadLocalZettelkasten();
  const currentPage = Number.parseInt(page);
  const pageCount = getZettelPages(zettelkasten);

  if (currentPage > pageCount) {
    throw new Error('all/[page].tsx(getStaticProps): currentPage is too large');
  }

  const zettels = await Promise.all(
    zettelkasten.zettels
      .slice(
        currentPage * config.zettelPerPage,
        (currentPage + 1) * config.zettelPerPage,
      )
      .map(({ id }) => getLocalZettelById(id)),
  );

  return {
    props: {
      siteTitle: config.title,
      siteDescription: config.description,
      title: config.title,
      description: config.description,
      zettels,
      currentPage,
      pageCount,
      header: {
        title: 'Archive',
        subtitle: `${currentPage + 1}/${pageCount}`,
      },
    },
  };
};

export const getStaticPaths: GetStaticPaths = async function getStaticPaths() {
  const zettelkasten = await loadLocalZettelkasten();

  const paths = [];
  const totalPages = getZettelPages(zettelkasten);
  for (let page = 0; page < totalPages; page++) {
    paths.push({
      params: {
        page: page.toString(),
      },
    });
  }

  return {
    paths,
    fallback: false,
  };
};
