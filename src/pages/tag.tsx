import React from 'react';
import config from '../data/config';
import Layout from '../components/layout/Layout';
import TagCloud from '../components/tag-cloud/TagCloud';
import labels from '../data/labels';
import { HeaderInterface } from '../components/header/Header';
import { GetStaticProps } from 'next';
import { loadLocalZettelkasten } from '../data/zettelkasten';

export interface TagPageInterface {
  title: string;
  description: string;
  tags: string[];
  header: HeaderInterface;
}

export const getStaticProps: GetStaticProps<TagPageInterface> = async function getStaticProps() {
  const zettelkasten = await loadLocalZettelkasten();

  return {
    props: {
      tags: zettelkasten.meta.tags,
      title: config.title,
      description: config.description,
      header: labels.pages.tag.header,
    },
  };
};

export default function TagPage(props: TagPageInterface): JSX.Element {
  return (
    <Layout
      siteTitle={props.title}
      siteDescription={props.description}
      header={props.header}
    >
      <TagCloud tags={props.tags} />
    </Layout>
  );
}
