import React from 'react';
import config from '../data/config';
import Layout from '../components/layout/Layout';
import Graph, { createZettelkastenSpec } from '../components/Graph';
import labels from '../data/labels';
import { HeaderInterface } from '../components/header/Header';
import { GetStaticProps } from 'next';
import { loadLocalZettelkasten } from '../data/zettelkasten';
import { VisualizationSpec } from 'react-vega';

export interface GraphPageInterface {
  title: string;
  description: string;
  header: HeaderInterface;
  spec: VisualizationSpec;
}

export const getStaticProps: GetStaticProps<GraphPageInterface> = async function getStaticProps() {
  const zettelkasten = await loadLocalZettelkasten();

  return {
    props: {
      title: config.title,
      description: config.description,
      header: labels.pages.graph.header,
      spec: createZettelkastenSpec(zettelkasten),
    },
  };
};

export default function GraphPage({
  title,
  description,
  header,
  spec,
}: GraphPageInterface): JSX.Element {
  return (
    <Layout siteTitle={title} siteDescription={description} header={header}>
      <Graph spec={spec} />
    </Layout>
  );
}
