import {
  faCode,
  faGlobe,
  faHashtag,
  faFolderOpen,
} from '@fortawesome/free-solid-svg-icons';
import { NavLinkInterface } from '../components/nav/Nav';
import config from './config';

const navLinks: NavLinkInterface[] = [
  {
    href: '/graph',
    title: 'Graph',
    icon: faGlobe,
  },
  {
    href: '/all/0',
    title: 'Archive',
    icon: faFolderOpen,
  },
  {
    href: '/tag',
    title: 'Tags',
    icon: faHashtag,
  },
  {
    href: config.repoUrl,
    title: 'Source code',
    target: '_blank',
    icon: faCode,
  },
];

export default navLinks;
