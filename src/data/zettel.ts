import { getZettelWithContent } from '@memorize/file-system';
import config from './config';
import { ZettelId, ZettelWithContentInterface } from '@memorize/types';

const zettels: Map<ZettelId, ZettelWithContentInterface> = new Map();

export async function getLocalZettelById(
  id: ZettelId,
): Promise<ZettelWithContentInterface> {
  if (zettels.has(id)) {
    return zettels.get(id);
  }

  const zettel = await getZettelWithContent(config.zettelkastenPath, id, false);
  zettels.set(id, zettel);
  return zettel;
}
