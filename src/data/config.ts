const config = {
  title: process.env.TITLE ? process.env.TITLE : 'Memorize',
  description: process.env.DESCRIPTION
    ? process.env.DESCRIPTION
    : 'Explore your information, Build your knowledge',
  zettelkastenPath: process.env.ZETTELKASTEN_PATH
    ? process.env.ZETTELKASTEN_PATH
    : './zettelkasten',
  zettelPerPage: process.env.ZETTEL_PER_PAGE
    ? Number.parseInt(process.env.ZETTEL_PER_PAGE)
    : 10,
  topZettelNumber: process.env.TOP_ZETTEL_NUMBER
    ? Number.parseInt(process.env.TOP_ZETTEL_NUMBER)
    : 3,
  repoUrl: process.env.REPO_URL
    ? process.env.REPO_URL
    : 'https://www.gitlab.com/memorize_it/memorize-website',
  creativeCommons: true,
};

export default config;
