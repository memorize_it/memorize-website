const labels = {
  pages: {
    tag: {
      header: {
        title: 'Tags',
        subtitle: 'Click to view all related zettels',
      },
    },
    archive: {
      header: {
        title: 'Archive',
        subtitle: 'Explore all your zettels',
      },
    },
    graph: {
      header: {
        title: 'Graph',
        subtitle: 'Graph visualization of your zettelkasten',
      },
    },
    index: {
      top: 'Top zettels',
      search: 'Search zettels',
    },
    zettel: {
      references: {
        by: 'Zettels quoting this zettel',
        byEmpty: 'There is no zettel quoting this zettel.',
        to: 'Quoted zettels',
        toEmpty: 'This zettel does not refer to any other zettel.',
      },
      seeGraph: 'See graph',
      tags: {
        tagsTitle: 'Tags',
        tagsEmpty: 'This zettel has no tags.',
      },
    },
  },
  components: {
    graph: {
      title: 'Graph visualization',
    },
    search: {
      placeholder: 'Zettel title',
    },
  },
};

export default labels;
