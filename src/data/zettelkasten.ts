import { loadZettelkasten } from '@memorize/file-system';
import config from './config';
import { ZettelkastenInterface } from '@memorize/types';

let zettelkasten: undefined | ZettelkastenInterface = undefined;

export async function loadLocalZettelkasten(): Promise<ZettelkastenInterface> {
  if (!zettelkasten) {
    zettelkasten = await loadZettelkasten(config.zettelkastenPath, false);
  }

  return zettelkasten;
}
