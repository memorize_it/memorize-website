import React from 'react';
import { storiesOf } from '@storybook/react';
import Colors from './Colors';

storiesOf('Colors', module).add('Palette', () => {
  return <Colors />;
});
