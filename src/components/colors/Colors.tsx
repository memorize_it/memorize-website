import React from 'react';
import style from './Colors.module.scss';

const colors = ['grey', 'green', 'yellow', 'orange', 'red'];
const shades = [10, 20, 30, 40, 50, 60];

function colorTile(
  color: string,
  tone?: string,
  shade?: number,
  text?: string,
) {
  const key = ['bg', color, tone, shade].filter(Boolean).join('-');
  return <div className={`${style.tile} ${key}`}>{text}</div>;
}

export default function Colors(): JSX.Element {
  return (
    <div className="container">
      {colors.map((color) => {
        return (
          <div className="row" key={color}>
            {[
              [...shades]
                .reverse()
                .map((shade) =>
                  colorTile(color, 'light', shade, shade.toString()),
                ),
              colorTile(color),
              [...shades].map((shade) =>
                colorTile(color, 'dark', shade, shade.toString()),
              ),
            ]}
          </div>
        );
      })}
      <hr />

      <div className="row">
        {[
          colorTile('yellow', '', 0, 'Primary'),
          colorTile('grey', '', 0, 'Secondary'),
          colorTile('green', '', 0, 'Success'),
          colorTile('red', '', 0, 'Danger'),
          colorTile('orange', '', 0, 'Warning'),
          colorTile('green', 'light', 30, 'Info'),
          colorTile('grey', 'dark', 60, 'Dark'),
          colorTile('grey', 'light', 60, 'Light'),
        ]}
      </div>
      <hr />

      <div className="row">
        {[
          colorTile('grey', 'dark', 20),
          colorTile('green', 'dark', 10),
          colorTile('yellow'),
          colorTile('orange', 'light', 10),
          colorTile('red', 'light', 20),
        ]}
      </div>
      <hr />

      <div className="row">
        {[
          colorTile('grey', 'dark', 40),
          colorTile('green', 'dark', 20),
          colorTile('yellow'),
          colorTile('orange', 'light', 20),
          colorTile('red', 'light', 40),
        ]}
      </div>
      <hr />

      <div className="row">
        {[
          colorTile('grey', 'dark', 40),
          colorTile('green', 'dark', 20),
          colorTile('yellow'),
          colorTile('orange', 'light', 10),
          colorTile('red', 'light', 20),
        ]}
      </div>
      <hr />

      <div className="row">
        {[
          colorTile('grey', 'dark', 40),
          colorTile('green', 'dark', 20),
          colorTile('yellow'),
          colorTile('orange', 'light', 10),
          colorTile('red', 'light', 20),
        ]}
      </div>
      <hr />
    </div>
  );
}
