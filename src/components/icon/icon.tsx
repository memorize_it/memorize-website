import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IconDefinition, SizeProp } from '@fortawesome/fontawesome-svg-core';

export interface ZettelListInterface {
  icon: IconDefinition;
  svgColorClass?: string;
  text?: string | number;
  size?: SizeProp;
  content?: JSX.Element;
}

export default function Icon({
  icon,
  text,
  size = 'xs',
  svgColorClass,
  content,
}: ZettelListInterface): JSX.Element {
  return (
    <div className="d-inline-flex mr-3 align-items-center">
      <div className="p-2">
        <FontAwesomeIcon className={svgColorClass} icon={icon} size={size} />
      </div>
      {text !== undefined ? (
        <span className={svgColorClass}>{text}</span>
      ) : undefined}
      {content ? content : undefined}
    </div>
  );
}
