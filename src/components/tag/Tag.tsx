import React from 'react';
import style from './Tag.module.scss';
import Link from 'next/link';

export interface TagInterface {
  tag: string;
  bgClass?: string;
  bgHoverClass?: string;
}

export default function Tag({
  tag,
  bgClass = 'bg-yellow',
  bgHoverClass = 'bg-orange--hover',
}: TagInterface): JSX.Element {
  return (
    <Link key={tag} href={`/tag/${tag}/0`}>
      <span className={`${style.tag__item} ${bgClass} ${bgHoverClass}`}>
        {tag}
      </span>
    </Link>
  );
}
