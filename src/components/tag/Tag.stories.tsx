import React from 'react';
import { storiesOf } from '@storybook/react';
import Tag from './Tag';

const stories = storiesOf('Tag', module);

'Lorem ipsum dolor sit amet'.split(' ').forEach((tag) => {
  stories.add(tag, () => {
    return <Tag tag={tag} />;
  });
});
