import React from 'react';
import style from './TagCloud.module.scss';
import Tag from '../tag/Tag';

export interface TagCloudInterface {
  tags: string[];
  center?: boolean;
  wrap?: boolean;
  bgClass?: string;
  bgHoverClass?: string;
}

export default function TagCloud({
  tags,
  center,
  wrap,
  bgClass,
  bgHoverClass,
}: TagCloudInterface): JSX.Element {
  const className = `d-flex m-0 p-0 ${
    center === false ? '' : 'justify-content-center'
  } ${wrap === false ? '' : 'flex-wrap'}`;

  function getTagItems() {
    return tags.length
      ? tags.map((tag) => (
          <li key={tag} className={style.cloud__item}>
            <Tag tag={tag} bgClass={bgClass} bgHoverClass={bgHoverClass}></Tag>
          </li>
        ))
      : undefined;
  }

  return <ul className={className}>{getTagItems()}</ul>;
}
