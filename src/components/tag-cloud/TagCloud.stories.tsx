import React from 'react';
import { storiesOf } from '@storybook/react';
import TagCloud from './TagCloud';

const shortTagList = 'Lorem ipsum dolor sit amet'.split(' ');
const longTagList = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum condimentum, nulla ut consequat pellentesque, mauris dui ultricies mauris, iaculis gravida metus diam sed diam. Mauris consequat tempus accumsan. Etiam pellentesque, quam at hendrerit semper, mi dui rhoncus orci, id pharetra risus ante at tellus. Maecenas lobortis sed nulla eget iaculis. Donec maximus massa lorem, et fermentum tortor cursus quis. Morbi viverra, nisi quis cursus scelerisque, diam sem finibus mi, in vestibulum justo elit eget est. Duis ac luctus turpis, eu blandit sapien. Quisque molestie augue ac neque fringilla ultrices. Vivamus auctor condimentum velit quis suscipit. Curabitur nisi odio, pellentesque eget nulla quis, pellentesque mollis neque. Mauris auctor erat ut rutrum blandit.'
  .replace(/\.|,/g, '')
  .split(' ');

storiesOf('TagCloud', module)
  .add('Short list', () => {
    return <TagCloud tags={shortTagList} />;
  })
  .add('Short list with background class', () => {
    return (
      <TagCloud
        tags={shortTagList}
        bgClass="bg-grey-light-60"
        bgHoverClass="bg-grey-light-20--hover"
      />
    );
  })
  .add('Short list not  centered', () => {
    return <TagCloud tags={shortTagList} center={false} />;
  })
  .add('Long list', () => {
    return <TagCloud tags={longTagList} />;
  })
  .add('Long list not wrapped', () => {
    return <TagCloud tags={longTagList} wrap={false} />;
  })
  .add('Long list not centered', () => {
    return <TagCloud tags={longTagList} center={false} />;
  })
  .add('Long list not centered nor wrapped', () => {
    return <TagCloud tags={longTagList} center={false} wrap={false} />;
  });
