import React, { PropsWithChildren } from 'react';
import style from './Layout.module.scss';
import Nav from '../nav/Nav';
import Meta from '../Meta';
import Footer from '../Footer';
import Header, { HeaderInterface } from '../header/Header';
import navLinks from '../../data/navLinks';

export interface LayoutInterface {
  siteTitle: string;
  siteDescription: string;
  header?: HeaderInterface;
}

export default function Layout({
  siteTitle,
  siteDescription,
  header,
  children,
}: PropsWithChildren<LayoutInterface>): JSX.Element {
  return (
    <>
      <Nav siteTitle={siteTitle} navLinks={navLinks} />
      <div className={style.layout__main}>
        <Meta siteTitle={siteTitle} siteDescription={siteDescription} />

        {header ? (
          <Header title={header.title} subtitle={header.subtitle} />
        ) : undefined}

        <div className="container h-100 d-flex flex-column justify-content-between">
          <div className="row flex-grow-1 justify-content-center">
            {children}
          </div>
          <Footer footer={siteTitle} />
        </div>
      </div>
    </>
  );
}
