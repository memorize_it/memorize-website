import React from 'react';
import { storiesOf } from '@storybook/react';
import ZettelList from './ZettelList';
import { mockZettels } from '../../mocks/zettel';
import { displayZettel } from '../zettel-list-item/ZettelListItem';
import { displayCompactZettel } from '../zettel-list-compact-item/ZettelListCompactItem';

storiesOf('Zettel List', module)
  .add('With title and compact', () => {
    return (
      <ZettelList
        zettels={mockZettels}
        title="Test tile"
        customRenderer={displayCompactZettel}
      />
    );
  })
  .add('Without title and normal', () => {
    return <ZettelList zettels={mockZettels} customRenderer={displayZettel} />;
  });
