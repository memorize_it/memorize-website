import React from 'react';
import styles from './ZettelList.module.scss';
import { ZettelInterface } from '@memorize/types';

export interface ZettelListInterface {
  title?: string;
  emptyList?: string;
  zettels: ZettelInterface[];
  customRenderer: (props: ZettelInterface) => JSX.Element;
}

export default function ZettelList({
  zettels,
  title,
  emptyList,
  customRenderer,
}: ZettelListInterface): JSX.Element {
  return (
    <div className="justify-content-center w-100">
      {title ? <h2 className="text-center">{title}</h2> : undefined}
      {zettels.length ? (
        <ul className={styles.list__container}>
          {zettels.length >= 1 &&
            zettels.map((zettel) => (
              <li key={zettel.id} className={styles.list__item}>
                {customRenderer({ ...zettel })}
              </li>
            ))}
        </ul>
      ) : (
        <p>{emptyList}</p>
      )}
    </div>
  );
}
