import React from 'react';
import styles from './ZettelListItem.module.scss';
import Zettel from '../zettel/Zettel';
import Link from 'next/link';
import { faSignOutAlt, faHashtag } from '@fortawesome/free-solid-svg-icons';
import Icon from '../icon/icon';
import TagCloud from '../tag-cloud/TagCloud';
import { ZettelWithContentInterface } from '@memorize/types';

export function displayZettel(props: ZettelWithContentInterface): JSX.Element {
  return <ZettelListItem {...props}></ZettelListItem>;
}

export default function ZettelListItem({
  id,
  references,
  tags,
  content,
}: ZettelWithContentInterface): JSX.Element {
  return (
    <div className="mb-4">
      <div>
        <Link href={`/${id}`}>
          <div className={styles.item__zettel}>
            <Zettel markdownBody={content} maxLength={200} noLinks={true} />
          </div>
        </Link>
      </div>
      <div className="d-flex">
        <Icon icon={faSignOutAlt} text={references.length} size="sm"></Icon>
        <Icon
          icon={faHashtag}
          size="sm"
          content={
            <TagCloud
              tags={tags.slice(0, 5)}
              bgClass="bg-grey-light-60"
              bgHoverClass="bg-grey-light-20--hover"
            ></TagCloud>
          }
        ></Icon>
      </div>
    </div>
  );
}
