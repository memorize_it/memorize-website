import React from 'react';
import { storiesOf } from '@storybook/react';
import { getMockZettelByTitle } from '../../mocks/zettel';
import ZettelListItem from './ZettelListItem';

storiesOf('Zettel List Item', module)
  .add('Short body', () => {
    const zettel = getMockZettelByTitle('Test Zettel 1');
    return <ZettelListItem {...zettel} />;
  })
  .add('Long body with long paragraphs', () => {
    const zettel = getMockZettelByTitle('Test Zettel 2');
    return <ZettelListItem {...zettel} />;
  })
  .add('With lists, links and tables', () => {
    const zettel = getMockZettelByTitle('Test Zettel 3');
    return <ZettelListItem {...zettel} />;
  })
  .add('With links to another zettels', () => {
    const zettel = getMockZettelByTitle('Test Zettel 4');
    return <ZettelListItem {...zettel} />;
  })
  .add('With emojis', () => {
    const zettel = getMockZettelByTitle('Test Zettel 5');
    return <ZettelListItem {...zettel} />;
  })
  .add('With emojis in the headers', () => {
    const zettel = getMockZettelByTitle('Test Zettel 6');
    return <ZettelListItem {...zettel} />;
  });
