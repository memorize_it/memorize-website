import React from 'react';
import { SignalListeners, Vega, VisualizationSpec } from 'react-vega';
import labels from '../data/labels';
import { VegaProps } from 'react-vega/lib/Vega';
import { Datum, Spec, ValuesData } from 'vega';
import {
  ZettelkastenInterface,
  ZettelkastenVisualizationEnum,
} from '@memorize/types';
import { getZettelkastenVisualizationSpec } from '@memorize/visualization';

export interface GraphInterface extends VegaProps {
  subtitle?: string;
}

export function handleClick(_: unknown, datum: Datum): void {
  if (!datum) {
    return;
  }

  window.location.href = datum.id;
}

export function createZettelkastenSpec(
  zettelkasten: ZettelkastenInterface,
): VisualizationSpec {
  const zettelkastenSpec = getZettelkastenVisualizationSpec(
    zettelkasten,
    ZettelkastenVisualizationEnum.FORCE,
  );

  const spec: Spec = {
    ...zettelkastenSpec,
    signals: [
      ...zettelkastenSpec.signals,
      {
        name: 'tooltip',
        value: {},
        on: [{ events: 'dblclick', update: 'datum' }],
      },
    ],
  };

  return spec;
}

export default function Graph({
  subtitle,
  ...props
}: GraphInterface): JSX.Element {
  const signalListeners: SignalListeners = { tooltip: handleClick };

  return (
    <div className="row">
      <div className="col text-center">
        <h2>{labels.components.graph.title}</h2>
        {subtitle ? <h3>{subtitle}</h3> : undefined}
        <Vega signalListeners={signalListeners} {...props} width={1000} />
      </div>
    </div>
  );
}
