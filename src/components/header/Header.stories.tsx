import React from 'react';
import { storiesOf } from '@storybook/react';
import Header from './Header';

storiesOf('Header', module)
  .add('Short text', () => {
    return <Header title="Lorem ipsum" />;
  })
  .add('Long text', () => {
    return (
      <Header title="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum condimentum, nulla ut consequat pellentesque, mauris dui ultricies mauris, iaculis gravida metus diam sed diam. Mauris consequat tempus accumsan. Etiam pellentesque, quam at hendrerit semper, mi dui rhoncus orci, id pharetra risus ante at tellus. Maecenas lobortis sed nulla eget iaculis. Donec maximus massa lorem, et fermentum tortor cursus quis. Morbi viverra, nisi quis cursus scelerisque, diam sem finibus mi, in vestibulum justo elit eget est. Duis ac luctus turpis, eu blandit sapien. Quisque molestie augue ac neque fringilla ultrices. Vivamus auctor condimentum velit quis suscipit. Curabitur nisi odio, pellentesque eget nulla quis, pellentesque mollis neque. Mauris auctor erat ut rutrum blandit." />
    );
  });
