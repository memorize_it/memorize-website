import React from 'react';
import style from './Header.module.scss';

export interface HeaderInterface {
  title: string;
  subtitle?: string;
  imageName?: string;
}

function Header({
  imageName = 'cover.jpg',
  title,
  subtitle,
}: HeaderInterface): JSX.Element {
  const assetPrefix = process.env.assetPrefix;

  const headerStyle: React.CSSProperties = {
    backgroundImage: `url('${[assetPrefix, imageName]
      .filter(Boolean)
      .join('/')}')`,
  };

  return (
    <header className={style.header__main} style={headerStyle}>
      <div className="container">
        <div className="row">
          <div className="col-lg-8 col-md-10 mx-auto">
            <div className={style.header__site}>
              <h1 className={style.header__title}>{title}</h1>
              {subtitle ? (
                <span className={style.header__subtitle}>{subtitle}</span>
              ) : undefined}
            </div>
          </div>
        </div>
      </div>
    </header>
  );
}

export default Header;
