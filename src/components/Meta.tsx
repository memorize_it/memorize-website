import React from 'react';
import Head from 'next/head';

export interface MetaInterface {
  siteTitle: string;
  siteDescription: string;
}

function Meta({ siteTitle, siteDescription }: MetaInterface): JSX.Element {
  return (
    <Head>
      <meta name="viewport" content="width=device-width, initial-scale=1" />
      <meta charSet="utf-8" />
      <title>{siteTitle}</title>
      <meta name="Description" content={siteDescription}></meta>
    </Head>
  );
}

export default Meta;
