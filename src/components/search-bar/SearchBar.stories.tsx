import React from 'react';
import { storiesOf } from '@storybook/react';
import SearchBar from './SearchBar';
import { mockZettels } from '../../mocks/zettel';

storiesOf('SearchBar', module).add('Zettels', () => {
  return <SearchBar zettels={mockZettels} />;
});
