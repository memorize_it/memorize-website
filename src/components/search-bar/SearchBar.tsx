import { ZettelInterface } from '@memorize/types';
import React, { ReactElement } from 'react';
import labels from '../../data/labels';
import { Typeahead } from 'react-bootstrap-typeahead';
import Link from 'next/link';

export interface SearchBarProps {
  zettels: ZettelInterface[];
}

export default function SearchBar({ zettels }: SearchBarProps): ReactElement {
  const renderZettel = (zettel: ZettelInterface) => (
    <li key={zettel.id}>
      <Link href={`/${zettel.id}`}>{zettel.title}</Link>
    </li>
  );

  const placeholder = labels.components.search.placeholder;

  return (
    <Typeahead
      id="zettel-search"
      labelKey="title"
      highlightOnlyResult={true}
      renderMenuItemChildren={renderZettel}
      placeholder={placeholder}
      options={zettels}
    />
  );
}
