import React, { PropsWithChildren } from 'react';
import Link from 'next/link';
import { faSignOutAlt, faHashtag } from '@fortawesome/free-solid-svg-icons';
import Icon from '../icon/icon';
import TagCloud from '../tag-cloud/TagCloud';
import { ZettelInterface } from '@memorize/types';

export function displayCompactZettel(args: ZettelInterface): JSX.Element {
  return (
    <ZettelListCompactItem {...args}>
      <Icon
        icon={faHashtag}
        size="sm"
        content={
          <TagCloud
            tags={args.tags.slice(0, 5)}
            bgClass="bg-grey-light-60"
            bgHoverClass="bg-grey-light-20--hover"
          ></TagCloud>
        }
      ></Icon>
    </ZettelListCompactItem>
  );
}

export function displayOneLineZettel(args: ZettelInterface): JSX.Element {
  return <ZettelListCompactItem {...args}></ZettelListCompactItem>;
}

export default function ZettelListCompactItem({
  references,
  title,
  id,
  children,
}: PropsWithChildren<ZettelInterface>): JSX.Element {
  return (
    <div className="d-flex flex-column">
      <div className="d-flex align-items-center justify-content-between">
        <Link href={`/${id}`}>
          <a>{title}</a>
        </Link>
        <Icon icon={faSignOutAlt} text={references.length} size="sm"></Icon>
      </div>
      <div>{children}</div>
    </div>
  );
}
