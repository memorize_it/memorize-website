import React from 'react';
import { storiesOf } from '@storybook/react';
import { getMockZettelByTitle } from '../../mocks/zettel';
import ZettelListCompactItem from './ZettelListCompactItem';

storiesOf('Zettel List Compact Item', module)
  .add('Short body', () => {
    const zettel = getMockZettelByTitle('Test Zettel 1');
    return <ZettelListCompactItem {...zettel} />;
  })
  .add('Long body with long paragraphs', () => {
    const zettel = getMockZettelByTitle('Test Zettel 2');
    return <ZettelListCompactItem {...zettel} />;
  })
  .add('With lists, links and tables', () => {
    const zettel = getMockZettelByTitle('Test Zettel 3');
    return <ZettelListCompactItem {...zettel} />;
  })
  .add('With links to another zettels', () => {
    const zettel = getMockZettelByTitle('Test Zettel 4');
    return <ZettelListCompactItem {...zettel} />;
  })
  .add('With emojis', () => {
    const zettel = getMockZettelByTitle('Test Zettel 5');
    return <ZettelListCompactItem {...zettel} />;
  })
  .add('With emojis in the headers', () => {
    const zettel = getMockZettelByTitle('Test Zettel 6');
    return <ZettelListCompactItem {...zettel} />;
  });
