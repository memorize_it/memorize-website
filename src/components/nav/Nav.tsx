import React from 'react';
import style from './Nav.module.scss';
import Link from 'next/link';
import { IconDefinition } from '@fortawesome/fontawesome-svg-core';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faMountain } from '@fortawesome/free-solid-svg-icons';

export interface NavLinkInterface {
  title: string;
  href: string;
  target?: string;
  icon: IconDefinition;
}

export interface NavInterface {
  siteTitle: string;
  navLinks: NavLinkInterface[];
}

export default function Nav({
  siteTitle,
  navLinks,
}: NavInterface): JSX.Element {
  return (
    <nav
      className={style.nav__bar}
      id="mainNav"
      role="navigation"
      aria-label="main navigation"
    >
      <ul className={style.nav__list}>
        <li className={style.nav__logo}>
          <Link href="/">
            <a className={style.nav__link}>
              <span className={style.nav__logoText}>{siteTitle}</span>
              <div className={style.nav__icon}>
                <FontAwesomeIcon
                  className={style.nav__logoIcon}
                  icon={faMountain}
                  size="2x"
                />
              </div>
            </a>
          </Link>
        </li>
        {navLinks.length >= 1 &&
          navLinks.map((h) => (
            <li key={h.title} className={style.nav__item}>
              <Link href={h.href}>
                <a className={style.nav__link} target={h.target}>
                  <div className={style.nav__icon}>
                    <FontAwesomeIcon icon={h.icon} size="2x" />
                  </div>
                  <span className={style.nav__text}>{h.title}</span>
                </a>
              </Link>
            </li>
          ))}
      </ul>
    </nav>
  );
}
