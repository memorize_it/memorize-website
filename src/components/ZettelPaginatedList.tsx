import React from 'react';
import ReactPaginate from 'react-paginate';
import { useRouter } from 'next/router';
import ZettelList from './zettel-list/ZettelList';
import { ZettelWithContentInterface } from '@memorize/types';
import { displayCompactZettel } from './zettel-list-compact-item/ZettelListCompactItem';

export interface ZettelPaginatedListInterface {
  zettels: ZettelWithContentInterface[];
  pageCount: number;
  currentPage: number;
  basePath: string;
}

export default function ZettelPaginatedList({
  zettels,
  pageCount,
  currentPage,
  basePath,
}: ZettelPaginatedListInterface): JSX.Element {
  const router = useRouter();

  function handlePageClick({ selected }): Promise<boolean> {
    return router.push(`${basePath}/${selected}`);
  }

  return (
    <div className="col">
      <ZettelList
        zettels={zettels}
        customRenderer={displayCompactZettel}
      ></ZettelList>
      <ReactPaginate
        previousLabel={'previous'}
        nextLabel={'next'}
        breakLabel={'...'}
        pageCount={pageCount}
        activeClassName="active"
        containerClassName="pagination justify-content-center"
        disabledClassName="disabled"
        pageLinkClassName="page-link"
        nextLinkClassName="page-link"
        previousLinkClassName="page-link"
        previousClassName="page-item"
        nextClassName="page-item"
        pageClassName="page-item"
        initialPage={currentPage}
        marginPagesDisplayed={2}
        pageRangeDisplayed={5}
        onPageChange={handlePageClick}
        disableInitialCallback={true}
      />
    </div>
  );
}
