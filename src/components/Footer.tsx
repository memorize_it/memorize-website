import React from 'react';
import config from '../data/config';
export interface FooterInterface {
  footer: string;
}

export default function Footer({ footer }: FooterInterface): JSX.Element {
  return (
    <footer>
      <div className="container">
        <div className="row">
          <div className="col-lg-8 col-md-10 mx-auto">
            <p className="text-center">Copyright &copy; {footer} 2020</p>
            {config.creativeCommons ? (
              <p className="text-center">
                <a
                  rel="license"
                  href="https://creativecommons.org/licenses/by/4.0/"
                >
                  <img
                    alt="Creative Commons License"
                    src="https://i.creativecommons.org/l/by/4.0/88x31.png"
                  />
                </a>
                <br />
                This work is licensed under a{' '}
                <a
                  rel="license"
                  href="https://creativecommons.org/licenses/by/4.0/"
                >
                  Creative Commons Attribution 4.0 International License
                </a>
                .
              </p>
            ) : undefined}
            <div className="row justify-content-center">
              <p>Created using-</p>
              <a href="https://gitlab.com/memorize_it/memorize" target="blank">
                Memorize
              </a>
            </div>
          </div>
        </div>
      </div>
    </footer>
  );
}
