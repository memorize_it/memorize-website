import React from 'react';
import ReactMarkdown from 'react-markdown';
import RemarkGFM from 'remark-gfm';
import RemarkGemoji from 'remark-gemoji';
import RemarkUnlink from 'remark-unlink';

export interface ZettelPropsInterface {
  markdownBody: string;
  maxLength?: number;
  noLinks?: boolean;
}

export default function Zettel({
  markdownBody,
  maxLength,
  noLinks,
}: ZettelPropsInterface): JSX.Element {
  if (!noLinks) {
    markdownBody = markdownBody.replace(
      /(\#[a-zA-Z0-9_\-]+)/gm,
      (t) => `[${t}](tag/${t.replace('#', '')}/0)`,
    );
  }

  if (maxLength && markdownBody.length > maxLength) {
    markdownBody = markdownBody.slice(0, maxLength).trimEnd().concat('...');
  }

  const plugins = [RemarkGFM, RemarkGemoji];

  if (noLinks) {
    plugins.push(RemarkUnlink);
  }

  return (
    <div className="h-100 d-flex flex-column">
      <ReactMarkdown plugins={plugins} linkTarget="_blank">
        {markdownBody}
      </ReactMarkdown>
    </div>
  );
}
