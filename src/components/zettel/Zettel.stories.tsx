import React from 'react';
import { storiesOf } from '@storybook/react';
import Zettel from './Zettel';
import { getMockZettelByTitle } from '../../mocks/zettel';

storiesOf('Zettel', module)
  .add('Short body', () => {
    const zettel = getMockZettelByTitle('Test Zettel 1');
    return <Zettel markdownBody={zettel.content} />;
  })
  .add('Long body with long paragraphs', () => {
    const zettel = getMockZettelByTitle('Test Zettel 2');
    return <Zettel markdownBody={zettel.content} />;
  })
  .add('Long body with long paragraphs and max length flag', () => {
    const zettel = getMockZettelByTitle('Test Zettel 2');
    return <Zettel markdownBody={zettel.content} maxLength={120} />;
  })
  .add('With lists, links and tables', () => {
    const zettel = getMockZettelByTitle('Test Zettel 3');
    return <Zettel markdownBody={zettel.content} />;
  })
  .add('With links to another zettels', () => {
    const zettel = getMockZettelByTitle('Test Zettel 4');
    return <Zettel markdownBody={zettel.content} />;
  })
  .add('With links to another zettels and noLinks flag', () => {
    const zettel = getMockZettelByTitle('Test Zettel 4');
    return <Zettel markdownBody={zettel.content} noLinks={true} />;
  })
  .add('With emojis', () => {
    const zettel = getMockZettelByTitle('Test Zettel 5');
    return <Zettel markdownBody={zettel.content} />;
  })
  .add('With emojis in the headers', () => {
    const zettel = getMockZettelByTitle('Test Zettel 6');
    return <Zettel markdownBody={zettel.content} />;
  });
