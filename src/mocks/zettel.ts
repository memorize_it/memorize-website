import { ZettelWithContentInterface } from '@memorize/types';
import dedent from 'dedent';

export const mockZettels: ZettelWithContentInterface[] = [
  `# Test zettel

    With a random short body.`,
  `# Test zettel

    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam mollis ut mauris vel hendrerit. Integer sed lobortis tortor, vel tincidunt sapien. Sed at fermentum erat, eget dignissim justo. Suspendisse at mattis mi. Donec nec ullamcorper sem. Maecenas vel neque sit amet nunc accumsan accumsan in eget nibh. Praesent libero tortor, blandit ut dui sed, aliquam pulvinar ipsum. Suspendisse potenti. Donec sit amet fringilla urna.

    Integer imperdiet at quam eu viverra. Proin sed nisi et metus lobortis suscipit. Nam augue leo, accumsan a bibendum vitae, tincidunt a lectus. Etiam sodales diam vel est rutrum vestibulum. Suspendisse sem tortor, scelerisque ac venenatis et, efficitur eget urna. Pellentesque scelerisque venenatis justo, ut vehicula ante malesuada id. Maecenas lacinia, erat at semper finibus, erat nunc feugiat sem, ac pretium libero leo nec urna. Aenean nec turpis malesuada, egestas purus et, posuere lectus. Quisque mi est, aliquet ac varius quis, feugiat vitae tortor. Donec in odio nibh. Proin et ultrices tellus. Mauris vitae faucibus sapien, eu molestie lectus. Vivamus auctor consectetur mauris. Mauris sit amet erat id metus commodo convallis.

    Nulla facilisi. Integer vestibulum placerat dui quis ullamcorper. Ut tristique odio nibh, id fringilla quam laoreet eget. Ut pellentesque lacus vel sapien ullamcorper, eget rhoncus dolor semper. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Etiam eleifend volutpat rhoncus. Integer a convallis tortor, ac efficitur tortor. Ut in feugiat lectus, vitae hendrerit odio. Cras convallis quam vel purus faucibus vestibulum. Etiam venenatis, mi in volutpat aliquet, mauris lacus pellentesque ligula, ac ultrices felis nulla ac quam. Suspendisse rhoncus tempus libero, eu semper massa aliquet sed. Nunc tempor maximus felis, eget congue nisi fermentum pulvinar. Nulla facilisi. Etiam vestibulum vitae velit faucibus congue. Phasellus nec turpis interdum, consequat nunc a, vestibulum lectus. Etiam auctor nisl vitae metus rutrum, nec convallis purus rutrum.`,
  `# Test zettel
        
    ## Links
    
    [Archive](https://archive.org)
        
    ## Lists

    * [ ] Todo
    * [x] Done

    ## Tables

    | a | b |
    | :-: | :-: |
    | A content | B content |`,
  `# Test zettel
        
    ## Links
    
    - [New Zettel](20200418173110)
    - [External link](https://archive.org)
        
    A sentence with a link : [External link](https://archive.org).
    
    [External link](https://archive.org)

    ## Tags

    #data_abstraction
    
    A sentence with a #tag.`,
  `# Test zettel

    :+1:
    :book:
    :cat:
    :smile:`,
  `# Test zettel :+1:
  
    ## Another header :book:
    ### And another header :cat:
    :smile:`,
].map((content, i) => ({
  id: 20210103112210 + i,
  metadata: {
    directory: './zettelkasten',
    filenameWithExtension: 'test-zettel.md',
  },
  references: [],
  relatedZettels: [],
  tags: ['test', 'zettel'],
  title: `Test Zettel ${i + 1}`,
  content: dedent(`---
  ---
  ${content}`),
}));

export function getMockZettelByTitle(
  zettelTitle: string,
): ZettelWithContentInterface {
  const mockZettel = mockZettels.find(({ title }) => title === zettelTitle);
  if (mockZettel) {
    return mockZettel;
  }

  throw new Error('Zettel not found');
}
