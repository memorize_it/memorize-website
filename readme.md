# Memorize - Website

This package is an example of how to create a website to navigate the zettelkasten. Using [Next.js](https://nextjs.org/) it will build an static site which allows to explore the zettelkasten using an interactive visual network.

You can check a live example [here](https://memorize_it.gitlab.io/memorize-website/).

## How do I create my website ? 🤔

### Using the CI

You can check an example on this [repo](https://gitlab.com/memorize_it/memorize-website-ci) and the live example [generated site](https://memorize_it.gitlab.io/memorize-website-ci/).

1. Create a `.gitlab-ci.yml` inside the repo containing your zettelkasten

```
image: node:14

stages:
  - deploy

pages:
  stage: deploy
  script:
    - mkdir tmp
    - mv * ./tmp || true
    - git clone --depth 1 https://gitlab.com/memorize_it/memorize-website.git
    - mkdir ./memorize-website/new-zettelkasten
    - mv ./tmp/* ./memorize-website/new-zettelkasten
    - cd memorize-website
    - yarn
    - export TITLE="Memorize CI";
    - export DESCRIPTION="Published just adding the CI";
    - export ZETTELKASTEN_PATH="./new-zettelkasten";
    - export ZETTEL_PER_PAGE="3";
    - export ASSET_PREFIX="https://memorize_it.gitlab.io/memorize-website-ci";
    - export BASE_PATH="/memorize-website-ci";
    - yarn build
    - mkdir ../public
    - mv ./out/* ../public
  artifacts:
    paths:
      - public
  only:
    - master
    - website

cache:
  paths:
    - node_modules/

```

2. Setup yout environment variables on the file.

3. Commit and... that's it ! Your knowledge is available for everyone to explore.

### 💻 Cloning the repo

1. Use the website package as the base for your site, you can just clone this project.

```bash
git clone git@gitlab.com:memorize_it/memorize-website.git
```

2. Setup the environment variables :

```bash
export TITLE="Memorize";
export DESCRIPTION="Explore your information, Build your knowledge";
export ZETTELKASTEN_PATH="./zettelkasten";
export REPO_URL="https://www.gitlab.io/memorize_it/memorize-website";
export ZETTEL_PER_PAGE="3";
export ASSET_PREFIX="https://memorize_it.gitlab.io/memorize-website";
export BASE_PATH="/memorize-website";
```

4. You can build your site using `yarn build`. This will generate an standalone `/out` directory including all the html, js and css necessary to display your zettelkasten.

## 📝 Developement

While you develop your site, you can use `yarn dev` to launch [next.js](https://nextjs.org/) on developement mode and have a live preview of your site.
