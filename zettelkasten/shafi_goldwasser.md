---
id: 20200418173235
tags: []
references: []
---
# Shafi Goldwasser

For transformative work that laid the complexity-theoretic foundations for the science of #cryptography and in the process pioneered new methods for efficient verification of mathematical proofs in complexity theory.
