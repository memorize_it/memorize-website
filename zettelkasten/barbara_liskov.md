---
id: 20200418173133
tags: []
references: []
---
# Barbara Liskov

For contributions to practical and theoretical foundations of programming language and system design, especially related to #data_abstraction, #fault_tolerance, and #distributed_computing .
