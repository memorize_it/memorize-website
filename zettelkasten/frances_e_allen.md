---
id: 20200418173047
tags: []
references: []
---
# Frances E. Allen

For pioneering contributions to the theory and practice of optimizing compiler techniques that laid the foundation for modern #optimizing_compilers and automatic parallel execution.
