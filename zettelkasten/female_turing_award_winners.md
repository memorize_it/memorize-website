---
id: 20200418172819
tags: []
references: []
---
# Female Turing Award winners

This is a list of all the #female Turing Award winners until 18/04/2020.

- [Frances E. Allen](20200418173047)
- [Barbara Liskov](20200418173133)
- [Shafi Goldwasser](20200418173235)

Hope it grows soon !
